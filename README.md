<h2>Tweeps Tutorial step by step to run this api</h2>



<h5>System dependencies</h5>

* Docker (https://www.docker.com/)
* Docker-compose
* Ruby 2.5
* Rails 5.2

<h5>Gems</h5>

* The <a href="<https://rubygems.org/gems/l6_http>">l6_http</a> is a gem to make http and https request developed by me. Check on <a href="<https://gitlab.com/vini.freire.oliveira/l6_http>"> GIT</a>.
* The following gems were used  to help in the tests
  * Spec
  * Webmock

<h5>Step by step to run the project in localhost using docker-compose</h5> 

1. Download the project from wetransfer

   ```bash
   www.ddd.ddd.ddd
   ```

2. Open the project folder

   ```bash
   cd tweets
   ```

3. Build the containers and install the dependecies

   ```dockerfile
   docker-compose run --rm app bundle install
   ```

4. Now, create database, generate migrations and run the seeds file if needed

   ```ruby
   docker-compose run --rm app bundle exec rails db:create db:migrate db:seed
   ```

5. Start the server with

   ```
   docker-compose up
   ```

   

 <h5>To run Rspec test results</h5>

1. To run the tweets service spec

   ```dockerfile
   docker-compose run --rm app bundle exec rspec spec/services/tweets_service_spec.rb
   ```

2. To run all spec files

   ```dockerfile
   docker-compose run --rm app bundle exec rspec spec
   ```



 <h5>Links</h5>

 <h5><a href="https://documenter.getpostman.com/view/110504/Rztpr8RJ">Api end points document</a></h5>

 <h5><a href="https://gitlab.com/vini.freire.oliveira">Git</a></h5>

 <h5><a href="https://www.linkedin.com/in/vinicius-freire-b53507107/">LikedIn</a></h5>



 <h5>Extra docker-compose commands</h5>

1. Start the container in shared folder

   ```
   docker-compose run --rm app bash
   ```

2. Start the ruby container in ruby console

   ```
   docker-compose run --rm app bundle exec rails c
   ```

3. Generate the model using docker-compose

   ```
   docker-compose run --rm app bundle exec rails generate model comment text:text user:references event:references
   ```

4. Generate the controller

   ```
   docker-compose run --rm app bundle exec rails g controller reports index show create update destroy
   ```

   