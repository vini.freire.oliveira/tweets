require 'spec_helper'
require 'json'
require './app/services/tweets_service'
 
describe 'Sample' do
  it 'tweets' do
    res = TweetsService.new(ENV['url'], "tweeps", {"Content-Type": 'application/json',"Username": ENV['HTTP_USERNAME']}).perform
    expect(res.is_a? Array).to eql(true)
    expect(res.count !=0 ).to eql(true)
  end

end