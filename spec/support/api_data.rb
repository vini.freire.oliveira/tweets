RSpec.configure do |config|
  file = File.read("public/sample.json")

  config.before(:each) do   
    stub_request(:get, /tweeps.locaweb/ )
    .with(headers: {
      'Accept'=>'*/*'
    }).to_return(status: 200, body: file, headers: {})
  end
end