require 'rails_helper'

RSpec.describe TweetsController, type: :controller do

  describe "GET #mentions" do

    before do
      get :mentions
    end

    it "returns http success" do
      get :mentions
      expect(response).to have_http_status(:success)
    end

    it "returns json with locaweb mention" do
      res = JSON.parse(response.body)
      expect(res.count).to eql(2)
    end
    
  end

  describe "GET #relevants" do
    
    before do
      get :relevants
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "returns json with locaweb mention" do
      res = JSON.parse(response.body)
      expect(res.count).to eql(2)
    end

    it "returns json in order" do
      res = JSON.parse(response.body)
      expect(res[0]["followers_count"] < res[1]["followers_count"]).to eql(false)
    end


  end


end
