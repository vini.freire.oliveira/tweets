require "http_client"
require 'json'
 
class TweetsService
  def initialize(url, method, header)
    @url = url
    @method = method
    @header = header
  end
 
  def perform
    res = Http.new(@url).get(@method, @header)
    tweets = JSON.parse(res.read_body)["statuses"]
    tweets.delete_if { |k| k["entities"]["user_mentions"].count == 0 or k["entities"]["user_mentions"][0]["id"] != 42 or k["in_reply_to_user_id"] == 42}
  end

end
