class TweetsController < ApplicationController
  before_action :set_tweets
  
  def mentions
    res = []
    @tweets.each do |t|
      content = {"followers_count": t["user"]["followers_count"], "screen_name": t["user"]["screen_name"], "profile_link": t["user"]["profile_image_url"], "created_at": t["created_at"] ,"link": t["source"], "retweet_count": t["retweet_count"], "text": t["text"] ,"favorite_count": t["favorite_count"]}
      res.push({t["user"]["name"] => content})
    end
    render json: res.sort_by {|h| [h[h.keys[0]][:followers_count], h[h.keys[0]][:retweet_count], h[h.keys[0]][:favorite_count]]}.reverse!
  end

  def relevants
    res = []
    @tweets.each do |t|
      res.push({"followers_count": t["user"]["followers_count"], "screen_name": t["user"]["screen_name"], "profile_link": t["user"]["profile_image_url"], "created_at": t["created_at"] ,"link": t["source"], "retweet_count": t["retweet_count"], "text": t["text"] ,"favorite_count": t["favorite_count"]})
    end
    render json: res.sort_by {|h| [h[:followers_count], h[:retweet_count], h[:favorite_count]]}.reverse!
  end

  private

  def set_tweets
    @tweets = TweetsService.new(ENV['url'], "tweeps", header).perform.map {|o| o.except!('coordinates','coordinates', 'favorited', 'truncated', 'contributors', 'geo', 'place', 'in_reply_to_status_id') }
  end

  def header
    {"Content-Type": 'application/json',"Username": ENV['HTTP_USERNAME']}
  end

end
