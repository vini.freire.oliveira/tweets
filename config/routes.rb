Rails.application.routes.draw do
  get 'most_relevants', to: 'tweets#relevants'
  get 'most_mentions', to: 'tweets#mentions'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
